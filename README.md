# Minimum Mess
This does not fully solve the request made by Amichay Oren, none of the projects has authentication (the goal is to reach lesson 20 with quick_tutorial, currently [working on it](https://bitbucket.org/vietthan/pyramidquicktutorial/src/main/)).

however, it does serve two projects in two different ports simply because of configuration changes regarding ports.

and it does use nginx to proxy_pass and serve the content on the specified localhost path.
# Run the two apps 
Download and navigate to the projects:
```bash
$ git clone git@bitbucket.org:vietthan/quick.git
$ cd quick
```

Run the first app:
```bash
$ cd quick_tutorial2/cc_starter
$ python3 -m venv env
$ env/bin/pip install -e .
$ env/bin/pserve development.ini --reload &
```

Run the second app:
```bash
$ cd ../../quick_tutorial3/cc_starter
$ python3 -m venv env
$ env/bin/pip install -e .
$ env/bin/pserve development.ini --reload &
```

### View the results

The first and second project now live in [http://localhost:6543/](http://localhost:6543/) and [http://localhost:6544/](http://localhost:6544/) respectively.

![two apps, two ports](messy1.png)

# Configure and run nginx
Assuming `nginx` is installed, there are two options to run it.

### Option 1: Replace your default config file
find out where the config file is located with:

```bash
$ nginx -t
```

Replace the `nginx.cong` file with the one [included](nginx.conf) in this repo.

Run nginx:

```bash
$ nginx
```

### Option 2: Run the repo's nginx config file
Return to the root of the repo and use:
```bash
$ nginx -c $(pwd)/nginx.conf
```

### View the results

[http://localhost/a](http://localhost/a) is now serving content of [http://localhost:6543/](http://localhost:6543/).

[http://localhost/b](http://localhost/b) is now serving content of [http://localhost:6544/](http://localhost:6544/).

![two apps, a and b](messy2.png)
