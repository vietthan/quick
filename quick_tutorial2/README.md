## Python virtual enviroment
Always need to run this for some reason, will learn:

```bash
$ export VENV=~/projects/quick_tutorial/env
$ python3 -m venv $VENV
```

## Run
### 00 cc_starter
```bash
$ cd cc_starter/
$ env/bin/pserve development.ini --reload
```

### 01 hello_world
```bash
$ cd hello_world
$ $VENV/bin/python app.py
```

### 02 package
```bash
$ cd package
$ $VENV/bin/python tutorial/app.py
```

> Note that the way we're running the app (`python tutorial/app.py`) is a bit of an odd duck. We would never do this unless we were writing a tutorial that tries to capture how this stuff works one step at a time. It's generally a bad idea to run a Python module inside a package directly as a script.

## 03 ini
``` bash
$ cd ini
$ $VENV/bin/pserve development.ini --reload
```
> The `pserve` application runner has a number of command-line arguments and options. We are using `--reload` which tells `pserve` to watch the filesystem for changes to relevant code (Python files, the INI file, etc.) and, when something changes, restart the application. Very handy during development.

## 04 debugtoolbar
```bash
$ cd debugtoolbar
$ $VENV/bin/pserve development.ini --reload 
```

## 05 unit_testing
```bash
$ cd debugtoolbar
$ $VENV/bin/pytest tutorial/tests.py -q
```